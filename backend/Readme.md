# AIDiagnostic
AI Diagnostic Library

#TODO

1. Data Base

   We have two choices for db
   SQL or NoSQL
   At the moment I think it is better to use MongoDB
   MongoDB is NoSQL which is scalable

2. The server

   The server app is being developed in ./RestfulAPI
   
3. The Machine Learning (ML) library

   The ML library is being developed in ./DiagnosticLib

4. Add GitLab CI auto tests



5. The front-end web app

   It is being developed in ./DiagnosticWeb


#Note 1
The master branch is protected. So we cannot commit directly into this branch.
One needs to work in another branch and then use a merge request in git lab.
In this way the pipline will be automatically run and wen all the tests pass it is safe to merge it to the master branch.


#Note 2
Use comments everywhere for everything. It is useful for new collaborators.




# Developer


## 1. Install Poetry

First install poetry dependency manager

```console
pip install poetry
```

By default, the dependencies are managed with [Poetry](https://python-poetry.org/), go there for more info.

## 2. Go to server side project

The project is located here `./backend/app/`

```console
cd ./backend
```


## 3. Create a new virtual environment
This created environment is a new and fresh environment in which there is no installed packages.
To do this following commands have been used;

To get started, if you’re not using Python 3, you’ll want to install the virtualenv tool with pip
```console
pip install virtualenv
```
To create a new environment

in linux and mac
```console
virtualenv -p `which python3` .pyenv
```

in windows
```console
py -m venv .pyenv
```

It creates a new environment in the folder .pyenv which can be activated by the following command (Note that pycharm activate it automatically when you open the project.)

in linux and mac
```console
source .pyenv/bin/activate
```
in windows
```console
.pyenv\Scripts\activate.bat
```
note thet .pyenv is in the .gitignore file, and the folder is not in the remote repository.


## 4. Installation


Make sure you are at `./backend/app/`. Then you can install all the dependencies using:

```console
cd ./app
poetry install
cd ..
```

## 4. Run and stop the whole project
To run the services we need [docker](https://docs.docker.com/engine/install/ubuntu/).

Open a new terminal. To run everything you should be at `/aidiagnostic`.

Note, docker commands should be run as sudoer or resolve it permanently [here](https://docs.docker.com/engine/install/linux-postinstall/).

```console
sudo -s
```
and use your password.

To run the services (For the first time it takes a while)

```console
docker-compose up -d
```

To stop the docker containers

```console
docker-compose down
```

## 5. To test everything

To run the tests you should be at `/aidiagnostic` as sudoer.

```console
bash ./scripts/test-local.sh
```

The script would stop all the running docker containers first.

## 6. Contribution
Guidelines for developers.

First, Fetch all remotes and update the project. [# git fetch, git pull]

The following shows how to make a copy of an existing branch (e.g., dev)

   1- right-click on a branch (e,g., dev)

   2- choose "New branch from selected"

   3- write a name (e.g., dev_issue_10) and press create.

Now you are in a new branch. When your issue is done, you need to merge a request as follows.

   1- commit [# git commit -m "comments"]

   2- push [# git push]

   3- merge 


