import os

if __name__ == "__main__":
    os.system('pycodestyle --ignore=E501,E701,E731,W504 --count ./app')
    os.system('pylint --errors-only --disable=no-self-argument ./app')
