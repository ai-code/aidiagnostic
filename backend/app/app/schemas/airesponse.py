from pydantic import BaseModel


class AIResponse(BaseModel):
    response: str
    elapsed_time: float
