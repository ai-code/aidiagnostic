from typing import Any
import time
from fastapi import APIRouter, HTTPException
from app import schemas
import numpy as np

router = APIRouter()


@router.post("/sin/{x}", response_model=schemas.AIResponse, status_code=201)
def ai_sin(
    x: float,
) -> Any:
    """
    Test sin function.
    """
    try:
        t_start = time.time()
        ans = np.sin(x)
        t_end = time.time()
        return {"response": ans, "elapsed_time": str(t_end - t_start)}
    except Exception as e:
        raise HTTPException(
            status_code=404,
            detail=str(e),
        )
