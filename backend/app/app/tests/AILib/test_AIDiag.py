import pytest
from sklearn import svm
from app.AILib.AIDiag import AIDiag


def test_init():
    aidiag_svc = AIDiag(AIDiag.Classifiers.SVC, None, C=200)
    assert aidiag_svc._classifier.gamma == 0.0001
    assert aidiag_svc._classifier.C == 200


def test_from_aidiag():
    assert 0 == 0


def test_from_file():
    assert 0 == 0


def test_from_ml_model():
    assert 0 == 0


def test_from_database():
    assert 0 == 0


def test_fit():
    assert 0 == 0


def test_save_as():
    assert 0 == 0


def test_store():
    assert 0 == 0


def test_load():
    assert 0 == 0


def test_append():
    assert 0 == 0


def test_predict():
    assert 0 == 0


def test_get_max_probs():
    assert 0 == 0
