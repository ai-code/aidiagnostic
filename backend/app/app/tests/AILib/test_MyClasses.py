import pytest

from app.AILib.MyClasses import MyClass as Cls


def test_sync_sum() -> None:
    obj1 = Cls()
    ans = obj1.sync_sum(a=1.0, b=2.0)
    assert ans == 3.0


@pytest.mark.asyncio
async def test_async_sum() -> None:
    obj1 = Cls()
    ans = await obj1.async_sum(a=1.0, b=2.0)
    assert ans == 3.0
