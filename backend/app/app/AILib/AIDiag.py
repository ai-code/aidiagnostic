# import asyncio
import copy
import psycopg2
import pickle
import numpy as np
from sklearn.impute import SimpleImputer
from enum import Enum
from sklearn import svm
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier


class AIDiag:
    class Classifiers(Enum):
        SVC = 1
        GaussianNB = 2
        RandomForestClassifier = 3

    def __init__(self, classifier, connection, gamma=0.0001, C=100, n_estimators=10):
        """
        init constructor
        :param classifier: classifier from scikit-learn
        :param connection: connection to psycopg2
        :return: AIDiag object
        """
        __classifier = None
        if classifier == self.Classifiers.SVC:
            __classifier = svm.SVC(gamma=gamma, C=C)
        elif classifier == self.Classifiers.SVC:
            __classifier = GaussianNB()
        elif classifier == self.Classifiers.SVC:
            __classifier = RandomForestClassifier(n_estimators=n_estimators)
        else:
            raise ValueError('Invalid classifier ...')
        self._classifier = __classifier
        if connection:
            self._cur = connection.cursor()
            self._connection = connection
        self._imputer = None

    def copy(self):
        """
        get a copy from AIDiag object
        :return: AIDiag object
        """
        return copy.deepcopy(self)

    def train(self, list_evidences, list_diseases):
        """
        predict based on evidences
        :param list_evidences: numpy array including data of evidences (float array)
        :param list_diseases: numpy array including data of disease ids (float integer array)
        """
        self._imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
        self._imputer = self._imputer.fit(list_evidences)
        list_evidences_imp = self._imputer.transform(list_evidences)
        self._classifier = self._classifier.fit(list_evidences_imp, list_diseases)

    def save_as(self):
        # A function to save the ML model to file
        return

    def load(self, file):
        # A function to save the ML model to file
        classifier = "to be done later"
        return classifier

    def store(self):
        """
        A method to store ML model to postgreSQL database.
        """
        data = pickle.dumps(self._classifier)
        sql = "INSERT INTO sampletable (epoch, file)  VALUES(%s)"
        self._cur.execute(sql, (psycopg2.Binary(data)))
        self._connection.commit()

        return

    def fetch(self):
        # A function to load the ML model from file
        classifier = "to be done later"
        return classifier

    def append(self):
        # A function to add some new learning to a loaded ML model
        return

    def predict(self, evidences):
        """
        predict based on evidences
        :param evidences: numpy array including data of evidences (float array)
        :return: ID of physical and mental diseases (integer array)
        """
        evidences_imp = self._imputer.transform([evidences])
        ids = self._classifier.predict(evidences_imp)
        return ids

    def get_max_probs(self, percentage):
        # A function that returns the most probabilities with respect to the threshold percentage
        return
