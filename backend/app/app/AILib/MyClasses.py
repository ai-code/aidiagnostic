import asyncio


class MyClass:
    def __init__(self):
        pass

    def sync_sum(self, a, b):
        """ A test function that returns the summation of a+b
        :param a: float
        :param b: float
        :return: a+b float
        """
        return a + b

    async def async_sum(self, a, b):
        """ An async test function that returns the summation of a+b
        :param a: float
        :param b: float
        :return: a+b float
        """
        await asyncio.sleep(2.0)
        return a + b
