#!/usr/bin/env bash

set -x

pycodestyle --ignore=E501,E701,E731,W504 --count ./app

